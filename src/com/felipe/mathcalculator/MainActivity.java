package com.felipe.mathcalculator;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	private static final String KEY_PLUS = "plus";
	private static final String KEY_MINUS = "minus";
	private static final String KEY_MULTIPLY = "multiply";
	private static final String KEY_DIVISION = "division";
	private static final String KEY_EQUAL = "equal";

	private TextView tv_result;

	private Button plus;
	private Button minus;
	private Button multiply;
	private Button division;
	private EditText et_num1;
	private EditText et_num2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initViews();
	}

	private void initViews() {
		plus = (Button) findViewById(R.id.plus);
		minus = (Button) findViewById(R.id.minus);
		multiply = (Button) findViewById(R.id.multiply);
		division = (Button) findViewById(R.id.division);
		et_num1 = (EditText) findViewById(R.id.et_num1);
		et_num2 = (EditText) findViewById(R.id.et_num2);
		tv_result = (TextView) findViewById(R.id.tv_result);
	}

	public void plus(View v) {
		if (et_num1.getTag() != null && et_num1.getTag() == KEY_PLUS) {
			// Se já tiver uma operação soma, ele não seta a operação novamente
		} else {
			if (et_num1.getText().toString() != "") {
				et_num1.setTag(KEY_PLUS);
				et_num2.setFocusable(true);
			}
		}
	}

	public void minus(View v) {
		if (et_num1.getTag() != null && et_num1.getTag() == KEY_MINUS) {
			// Se já tiver uma operação soma, ele não seta a operação novamente
		} else {
			if (et_num1.getText().toString() != "") {
				et_num1.setTag(KEY_MINUS);
				et_num2.setFocusable(true);
			}
		}
	}

	public void multiply(View v) {
		if (et_num1.getTag() != null && et_num1.getTag() == KEY_MULTIPLY) {
			// Se já tiver uma operação soma, ele não seta a operação novamente
		} else {
			if (et_num1.getText().toString() != "") {
				et_num1.setTag(KEY_MULTIPLY);
				et_num2.setFocusable(true);
			}
		}
	}

	public void division(View v) {
		if (et_num1.getTag() != null && et_num1.getTag() == KEY_DIVISION) {
			// Se já tiver uma operação soma, ele não seta a operação novamente
		} else {
			if (et_num1.getText().toString() != "") {
				et_num1.setTag(KEY_DIVISION);
				et_num2.setFocusable(true);
			}
		}
	}

	public void equals(View v) {
		if (et_num1.getText().toString() != ""
				&& et_num2.getText().toString() != "" && et_num1.getTag() != null) {
			double num1 = Double.parseDouble(et_num1.getText().toString());
			double num2 = Double.parseDouble(et_num2.getText().toString());
			double result = 0;
			if (et_num1.getTag().equals(KEY_PLUS)) {
				result = num1 + num2;
				tv_result.setText("RESULTADO:  " + result);
			} else if (et_num1.getTag().equals(KEY_MINUS)) {
				result = num1 - num2;
				tv_result.setText("RESULTADO:  " + result);
			} else if (et_num1.getTag().equals(KEY_MULTIPLY)) {
				result = num1 * num2;
				tv_result.setText("RESULTADO:  " + result);
			} else {
				result = num1 / num2;
				tv_result.setText("RESULTADO:  " + result);
			}
			et_num1.setText("");
			et_num1.setTag("");
			et_num2.setText("");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
